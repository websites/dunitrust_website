# Dunitrust website

Pour développer en local, installez simplement zola et lancez

```
zola serve
```

dans le dossier dunitrust_website.

Consultez ensuite [http://localhost:1111/](http://localhost:1111/) dans votre navigateur pour admirer le site.

![aperçu](./aperçu.png)

> aperçu du site

## Deploiement

Le site est conçu pour résider à la racine de son nom de domaine. Pour déployer, construisez le site avec

```
zola build --base_url example.com
```

enfin, poussez vos fichiers statiques avec

```
rsync -avc --delete public/ http://example.com:/var/www/
```

Vous pouvez consulter une preview du site à l'adresse : [http://dunitrust.trentesaux.fr/](http://dunitrust.trentesaux.fr/).

## TODO

- page pour les testeurs
- design mobile
- anglais ?
