+++
title = "Construction du site web"
date = 2019-09-28

[extra]
illustration = "zola.png"
+++

La construction du site web de Dunitrust est en cours, j'apprends petit à petit à utiliser Zola, qui est très "opinonated" comme il le dit, mais fait des choix compatibles avec mon fonctionnement et qui me semblent pertinents.
