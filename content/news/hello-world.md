+++
title = "Hello World!"
date = 2019-09-27

[extra]
illustration = "rustacean-flat-sad.svg"
summary = "Ce résumé remplace le résumé par défaut de l'article."
+++

Bonjour, voici la première news sur le site web de Dunistrust.
