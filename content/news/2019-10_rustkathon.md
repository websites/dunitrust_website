+++
title = "Compte rendu du Rustkathon"
date = 2019-10-05

[extra]
illustration = "dunitrust_green-bg.png"
summary = "C’était une grande réussite avec 8 participants en présentiel très motivés, soit une augmentation de 33% par rapport a la 1ère édition :smiley:"
+++

C’était une grande réussite avec 8 participants en présentiel très motivés, soit une augmentation de 33% par rapport a la 1ère édition :smiley:

Cette 2ème édition comportait principalement des ateliers par équipe et du peer programming, les dev les plus expérimentés accompagnant les moins expérimentés, ce qui est infiniment plus formateur qu’une série de conférences (c’est par la pratique qu’on apprend).
Ce format donne moins de contenu pour ceux qui ne sont pas présents, mais est beaucoup plus enrichissant pour les participants.

Chacun a pu progresser sur des aspects où le besoin se faisait sentir, chacun formé par un autre s’avérant avoir la compétence ou l’expérience nécessaire.

Jonas a beaucoup manipulé la BDD, ce qui lui sera indispensable pour réaliser GVA, Hugo a appris l’approche TDD grace à Milicent, Ji_emme a progressé sur le langage Rust grâce a Jonas, Tuxmain a appris PyO3, etc...

De mon coté j’ai également appris, et tout particulièrement avec Milicent qui est très expérimenté en approche TDD et m’a donné de bien précieux conseils pour « monter en qualité » le projets.

Tout les moments off ont également été très agréables et participent à la constitution et consolidation d’un véritable équipe :smiley:

Chacun dispose hélas de très peu de temps libre, et l’apprentissage est long autant de Rust que du métier de Duniter/G1 qui est objectivement très compliqué.

Je fais le pari que ces évènements ainsi que mes investissements en temps de formation et d’accompagnement porteront leurs fruits à moyen-terme, même si à court-terme cela a pour conséquence que j’avance moins vite sur le projet.

« Tout seul on va plus vite , ensemble on vas plus loin »
