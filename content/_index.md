+++
title = "Accueil"
template = "landing.html"

[extra]
dunitrust_current = "Version 0.3"
dunitrust_beta = "Version 0.3 beta"
dunitrust_release = "https://git.duniter.org/nodes/rust/duniter-rs/-/releases"
+++
