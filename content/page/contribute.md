+++
title = "Participer"
+++

Pour participer financièrement (en ğ1 ou autre), allez voir [le site d'Axiom Team](https://manage.axiom-team.fr/).

Pour contribuer au code, inscrivez-vous sur le [forum duniter](https://forum.duniter.org/) ou sur le [GitLab Duniter](https://git.duniter.org/).
