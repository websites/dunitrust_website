+++
title = "Axiom team"
+++

Axiom Team est l'association qui soutient le projet Dunitrust et le développement de la monnaie libre à échelle globale.

[https://axiom-team.fr/](https://axiom-team.fr/)
