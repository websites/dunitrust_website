+++
title = "Signaler un problème"
+++

Si vous trouvez un problème sur le site lui-même, vous pouvez [ouvrir une issue](https://git.duniter.org/websites/dunitrust_website/issues) sur le dépôt du site.

S'il s'agit d'un bug sur le logiciel Dunitrust, vous pouvez [ouvrir une issue](https://git.duniter.org/nodes/rust/duniter-rs/issues) sur le dépôt de Dunitrust.
