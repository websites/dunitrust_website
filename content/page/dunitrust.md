+++
title = "Dunitrust"
+++

Le logiciel Dunitrust est une implémentation en Rust du protocole Duniter. La première implémentation de ce protocole, [Duniter](https://duniter.org/fr/), écrite en TypeScript est aujourd'hui la version dominante dans le réseau. C'est une preuve de concept qui a vécu les différentes phases de maturation du protocole et qui nécessite une refonte. Dunitrust se lance dans cette réécriture en Rust, un langage particulièrement adapté aux besoins d'une cryptomonnaie. Dunitrust permettra dans un premier temps de mitiger l'importance d'un bug dans le logiciel Duniter et à terme de le remplacer si ce dernier venait à ne plus être maintenu.

Le nom *Dunitrust* vient de *Duniter* et de *Rust*. Il contient le mot anglais *trust*, qui réfère à la toile de confiance, et le sigle *DU* du dividende universel.

Le langage Rust a été choisi pour un ensemble de raisons à la fois philosophiques, techniques, et stratégiques. Ce langage relativement récent a été développé dans la philosophie du logiciel libre par la fondation Mozilla afin d'améliorer les performances et la sécurité de Firefox. Le succès qui en a résulté a valu au langage beaucoup d'intérêt à la fois de la part développeurs et des grandes entreprises. Plusieurs personnes ont en effet profité du projet Dunitrust pour apprendre ce langage.

Le fait de travailler sur un protocole déjà éprouvé nous permet de faire des choix éclairés avec une vision sur le long terme. Dunitrust propose ainsi une architecture logicielle modulaire et évolutive, capable d'accueillir l'évolution de la monnaie. Les différents clients déjà en fonctionnement ont exprimé des demandes auxquelles répondra une API GraphQL, plus adaptée que REST aux problématiques rencontrées. Le découpage en différentes *crates* Rust permettra à d'autres programmes d'embarquer des fonctionnalités précises, comme la gestion des documents utilisateurs et des couches de cryptographie. Leur utilisation sera possible via des bindings vers d'autres langages ou une compilation en webassembly, permettant l'utilisation dans le navigateur.

{% iframe(src="https://librelois.duniter.io/slides/rustkathon2019_2/dunitrust/#1", ratio="4/3", link=true) %}
Ces slides décrivent le projet Dunitrust.
{% end %}
