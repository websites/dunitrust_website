+++
title = "À propos"
+++

Ce site a pour objectif de présenter le logiciel Dunitrust. Parce que nous aimons vraiment le Rust, nous avons choisi le générateur de site statique [Zola](https://www.getzola.org/) pour le réaliser. Il intègre directement la compilation du [Sass](https://sass-lang.com/) "CSS with superpowers" ce qui nous permet d'intégrer [Bulma](https://bulma.io/), un framework CSS écrit en Sass. Les icones sont fournies par [forkawesome](https://forkaweso.me/Fork-Awesome/icons/).

Les sources de ce site sont disponibles sur le [GitLab du projet Duniter](https://git.duniter.org/websites/dunitrust_website). Vous pouvez y suggérer des modifications.
