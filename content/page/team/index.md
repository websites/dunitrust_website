+++
title = "Participants"

[extra]
stylesheet = "/team.css"
+++

Sont présentés ici toutes les personnes ayant participé, même légèrement, au projet Dunitrust. Vous pouvez retrouver leurs contributions sur le git du projet.

---

<div class="team">

{% team_member(image="elois.jpg", website="https://librelois.fr/") %}
**Elois** est l'architecte du projet. Accueillir des contributeurs est sa priorité car il souhaite que le projet Dunitrust dure longtemps.
{% end %}

{% team_member(image="hugo.jpg", website="https://trentesaux.fr/") %}
**Hugo**, doctorant en neurosciences, membre de la toile francilienne, contribue au module WS2Pv2.
{% end %}

{% team_member(image="jonas.png") %}
**Jonas**, développeur Scala, contribue à l'API GVA.
La ML est pour lui un projet qui a du sens et Dunitrust une occasion de pratiquer le Rust,
langage fortement typé hors machine virtuelle.
{% end %}

{% team_member(image="tuxmain.svg", website="https://zettascript.org/") %}
**tuxmain**, lycéen, porte PKSTL du Rust au Python, a fait les paquets ArchLinux.
{% end %}

{% team_member(image="default.png", website="http://www.ethicnolo.gy/") %}
**Jules**, alias ethicnology, est étudiant en alternance à l'ESGI sur l'ingéniérie blockchain.
Travaille en R&D blockchain sur le marché de la dette, et souhaiterais contribuer à un système monétaire plus juste.
{% end %}

{% team_member(image="krousky.jpg") %}
**Krousky**, freelance ingénieur système industriel
{% end %}

{% team_member(image="JM.png") %}
**Jean-Marie**, alias JM,
Développeur Java, commence sur GVA.
{% end %}

{% team_member(image="1000i100.jpg", website="https://1forma-tic.fr/") %}
**1000i100**, jusque là contributeur à la ML plutôt côté JS, met au service de Dunitrust ses
compétences de développeur acquises à la fac à Bordeaux et de manière autodidacte.
{% end %}

{% team_member(image="neodarz.png", website="https://neodarz.net/") %}
**neodarz**, developpeur python, avait envie de contribuer à Duniter, un projet qui a du sens... mais pas en JS
{% end %}

{% team_member(image="lucas.png") %}
**Lucas**, chercheur en théorie des graphes, s'intéresse de près à Dunitrust.
{% end %}

</div>
