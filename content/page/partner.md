+++
title = "Partenaires"
+++

Le projet Dunitrust est une opportunité pour accueillir des contributeurs scolaires et universitaires sur le thème de la monnaie libre. Nous faisons ici un état des partenariats existants et de ceux que nous aimerions établir.

- le laboratoire [complex networks](https://www.complexnetworks.fr/) a publié une offre de stage ["Robustness of Web of Trust Mechanisms"](https://www.complexnetworks.fr/wp-content/uploads/2019/10/2019_internship_description_Gensollen.pdf) dans l'idée de mieux comprendre les possibilités et limites de la toile de confiance ğ1
- l'association [42l](https://42l.fr/) nous aide à faire le lien avec l'école 42, afin d'aborder les thématiques de l'écosystème logiciel de la ğ1 auprès des étudiants

- nous sommes à la recherche de partenaires pour une étude formelle des règles du protocole DUBP. Pour l'instant, [Martin Bodin](https://people.irisa.fr/Martin.Bodin/index.html), [Lesly-Ann Daniel](http://perso.eleves.ens-rennes.fr/people/Lesly-ann.Daniel/) et Florent Chevrou ont montré leur intérêt
- nous allons proposer aux étudiants de Epita, Epitech, 42, et l'UTC de travailler sur un point précis de Dunitrust en tant que projet de fin d'étude. Les points restent à défnir
