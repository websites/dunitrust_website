+++
title = "Rustkathon octobre 2019"
date = 2019-10-05

[extra]
illustration = "dunitrust_green-bg.png"
summary = "Le Rustkathon aura lieu à Toulouse pendant le weekend du 5 et 6 octobre."
+++


![affiche rustkathon](rustkathon_oct19.jpeg)

2ème édition du rustkathon les **5 et 6 octobre 2019** dans le centre de **Toulouse** ![:smiley:](https://forum.duniter.org/images/emoji/twitter/smiley.png?v=9)
 C’est un hackathon dédié au projet [**Dunitrust** 3](https://forum.duniter.org/t/etat-davancement-de-dunitrust-nouveau-nom-de-durs/6384).

Inscriptions par mail **rustkathon2019@axiom-team.fr**

**Prérequis**

- Avoir visionné la présentation du langage Rust par  [@nanocryk](https://forum.duniter.org/u/nanocryk) lors du 1er rustkathon:  [**peertube** 5](https://peertube.librelois.fr/videos/watch/6d8c9858-23b4-41d8-b968-7de01d15ca79) / **youtube**
   (nous ne referons pas une présentation aussi longue et complète du  langage Rust, mais bien entendu vous  pourrez poser des questions sur le  langage).
- Avoir visionné la présentation du protocole DUP lors du 1er rustkathon : [**peertube**](https://peertube.librelois.fr/videos/watch/8e572a40-1af1-4196-9e70-336c00c46a35) /  [**youtube** 1](https://www.youtube.com/watch?v=1ZOb7XDk3Dc)
- Avoir un trousse de clés ssh (cf. programme)
- Savoir déjà coder dans au moins 1 langage de programmation  (développeurs front acceptés). Nul besoin d’être un expert, c’est juste  que si vous venez pour découvrir la programmation ce n’est pas  l’endroit. Vous devez déjà être a l’aise avec les notions de bases de la  programmation (variables, fonctions, etc)

**Inscription**

Inscription par mail **rustkathon2019@axiom-team.fr**
 **Les participants en remote doivent également s’inscrire** (accès au serveur mumble pour parler avec nous en direct).

Merci de joindre une clé ssh publique a votre demande d’inscription,  et de venir a l’événement avec une machine possédant la clé ssh privée  correspondante.

**Ébauche de programme**

- Point d’avancement sur le projet Dunitrust, jalon 0.3 prévu pour les rml14
- Containerized development: atelier pratique de développement dans un  conteneur. Le principe est d’avoir un environnement de développement  complet et a jours sans se prendre la tête.
- Développement dans le cloud. Compilation trop lente sur votre  machine ? Atelier pratique de déploiement d’un poste de dev dans le  cloud afin de disposer d’une machine puissante et donc d’une compilation  rapide (utilise le Containerized development).
- Test Driven Développement (TDD): Atelier pratique de TDD en Rust.  Développement d’une feature dans Dunitrust avec l’approche TDD.
- Présentation des stratégies de test du projet Dunitrust : ce qui est déjà en place et ce qui est prévu à court terme.
- Cryptographie ed25519: établir une communication chiffrée par échange DH
- Et quelques autres surprises ![:smiley:](https://forum.duniter.org/images/emoji/twitter/smiley.png?v=9)

**Détails pratiques**

Hébergement sur place pour 100 Ğ1/nuit (repas non compris)

Accueil dés le vendredi soir à partir de **21h** (adresse fournie uniquement aux inscrits).
 Début officiel de l’événement le Samedi à **10h** pétante. Merci d’arriver dés 9h30 de préférence.
 Fin officielle de l’événement le Dimanche à **17h** (pour ceux qui ont de la route).
 Personnellement je serait encore sur place jusqu’à lundi soir, de quoi aller plus loin avec ceux qui le souhaitent ![:blush:](https://forum.duniter.org/images/emoji/twitter/blush.png?v=9)

Tout comme pour la 1ère édition, il sera possible de participer en  remote, mais ça reste beaucoup plus enrichissant en présentiel ![:wink:](https://forum.duniter.org/images/emoji/twitter/wink.png?v=9)

Dans le mesure du possible, nous diffuserons en direct sur [la chaine youtube du rustkathon 1](https://www.youtube.com/channel/UCmkzVFUSdP_I2NP4bQtmRzA) (selon l’état de la connexion internet).
 Les replay seront publiés sur la chaine YT et sur [**la chaine peertube** 2](https://peertube.librelois.fr/accounts/rustkathon/videos).
