+++
title = "Conférence aux 42l novembre 2019"
date = 2019-11-13

[extra]
illustration = "events/19-11-conf-42l/logo_42l.svg"
summary = "Une conférence organisée par l'association 42l aura lieu à Paris le 13 novembre."
+++
